# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Fredkindings@bitbucket.org/Fredkindings/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Fredkindings/stroboskop/commits/bd1579e829f27de124e752f1cc292b387380be5e
https://bitbucket.org/Fredkindings/stroboskop/commits/31dadc9b8c962fb531e242184c90f613b0dcf92f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Fredkindings/stroboskop/commits/ed0d81fdec088e85b562f3d2b02e2f87d581c2cc

Naloga 6.3.2:
https://bitbucket.org/Fredkindings/stroboskop/commits/67498446941790a5645164fdb95a9073c3997611

Naloga 6.3.3:
https://bitbucket.org/Fredkindings/stroboskop/commits/045abb803f70a9bbb580714109e46a0d2f584dc2

Naloga 6.3.4:
https://bitbucket.org/Fredkindings/stroboskop/commits/17e667dfd962ff92c411b960377162a9bf58df38

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push --all
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Fredkindings/stroboskop/commits/0ac0c2f2b814a2ca277369fde29f18056ff8e507

Naloga 6.4.2:
https://bitbucket.org/Fredkindings/stroboskop/commits/3df8ed77f4168abcae7b896e589cbdd367fbeea4

Naloga 6.4.3:
https://bitbucket.org/Fredkindings/stroboskop/commits/880385cde6e07217feef4cd18147ecade5e50fcb

Naloga 6.4.4:
https://bitbucket.org/Fredkindings/stroboskop/commits/3d68e914154801e233415cf2771dc475526cd8f0?at=dinamika